import Vuex from 'vuex';
import Vue from 'vue';
import mutations from './mutations';

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    check: {
      bgColor: '#ddd',
      bgImg: ''
    }
  },
  mutations
});

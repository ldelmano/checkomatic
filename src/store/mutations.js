export default {
  changeColor(state, color) {
    state.check.bgColor = color;
  },
  changeBgImg(state, img) {
    state.check.bgImg = img.url;
  }
}

import locale from 'element-ui/lib/locale/lang/en';
import Vue from 'vue';

import VueRouter from 'vue-router';
import { routes } from './router/index';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { store } from './store';

import App from './App.vue';


Vue.use(ElementUI, { locale })
Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
